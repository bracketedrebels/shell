"use strict";

var AbstractModule = require("./base/AbstractModule.js");
var Path = require("path");
var FS = require("fs");
var MetaMarked = require("meta-marked");
var Prince = require("prince");
var UnZIP = require("unzip");

class ModuleMD2PDF extends AbstractModule {
    constructor() {
        super("md2pdf");
        this._resetFieldsState();
        var lRenderer_obj = new MetaMarked.Renderer();
        lRenderer_obj.heading = ModuleMD2PDF.renderHeading;
        var lNativeTableCellRenderer_func = lRenderer_obj.tablecell;
        var lNativeImageRenderer_func = lRenderer_obj.image;
        lRenderer_obj.tablecell = function (content, flags) {
            content = content.replace(/\\n/gm, "<br/>");
            return lNativeTableCellRenderer_func.call(lRenderer_obj, content, flags);
        };
        lRenderer_obj.image = function (href, title, text) {
            var lMatchResults = text.match(/^(\d+%)/);
            var lPrefix_str = lMatchResults ? '<div class="image" style="max-width: ' + lMatchResults[1] + '">' : '<div class="image">';
            if (lMatchResults) {
                text = text.substring(lMatchResults.index + lMatchResults[1].length).trim();
            }
            return lPrefix_str + lNativeImageRenderer_func.call(lRenderer_obj, href, title, text) + '<div class="title">' + title + '</div></div>';
        };
        MetaMarked.setOptions({
            renderer: lRenderer_obj,
            xhtml: true
        });
    }

    _resetFieldsState() {
        this._fTargetFileContent_str = null;
        this._fPathToOutputHTML_str = null;
        this._fPathToOutputPDF_str = null;
        this._fTADsToUpdateIndicies_num_arr = [];
    }

    __runCommand() {
        this._resetFieldsState();
        this._fPathToOutputHTML_str = Path.join(Path.dirname(this.__fOptions_obj.md), "output.html");
        this._fPathToOutputPDF_str = Path.join(this.__fOptions_obj.out, this.__fOptions_obj.name || Path.basename(this.__fOptions_obj.md, ".md")) + ".pdf";
        this._fTargetFileContent_str = String(FS.readFileSync(this.__fOptions_obj.md, 'utf8'));
        this._compileMDToHTML();
    }

    __registerCommand(aNomNomCommand_obj) {
        var lFileValidator_func = ModuleMD2PDF._validateFile;
        var lInputValidator_func = this.__validateFile.bind(this);
        aNomNomCommand_obj
            .option('md', {
                required: true,
                help: "source file with markdown formatted text",
                transform: lInputValidator_func
            })
            .option('out', {
                abbr: 'o',
                help: "path to directory, where compiled pdf should be placed",
                default: Path.resolve(process.cwd()),
                transform: lFileValidator_func
            })
            .option('style', {
                abbr: 's',
                help: "path to style archive (style.css in the root)",
                transform: lInputValidator_func
            })
            .option('name', {
                abbr: "n",
                help: "name of output pdf file. By default it will use the same name as original md file"
            })
            .help("Renders markdown formatted text into pdf file, using provided stylesheet");
    }

    _compileMDToHTML() {
        var aCompiled_obj = MetaMarked(this._fTargetFileContent_str);
        var lHTML_str = this._proceedCompiledMDWithTemplate(aCompiled_obj.html, aCompiled_obj.meta);
        var lPathToOutputHTMLFile_str = this._fPathToOutputHTML_str;
        FS.writeFile(lPathToOutputHTMLFile_str, lHTML_str, this._onHTMLFileWritten.bind(this));
    }

    _onHTMLFileWritten(aError_obj) {
        if (aError_obj) {
            this.__commandFailed(aError_obj);
        }
        this._decompressTemplateIfRequired();
    }

    _proceedCompiledMDWithTemplate(aHTML_str, aMetaInformation_obj) {
        var lHeader_str = this._makeHeader(aMetaInformation_obj);
        var lBodyPrepender_str = this._makeBodyPrepender(aMetaInformation_obj);
        return "<html>" + lHeader_str + "<body>" + lBodyPrepender_str + aHTML_str + "</body></html>";
    }

    _makeHeader(aMetaInformation_obj) {
        var lHeader_obj = {
            head: {
                meta: [{
                    $: {
                        charset: "utf-8"
                    }
                }]
            }
        };
        if (this.__fOptions_obj.style) {
            lHeader_obj.head.link = [{
                $: {
                    rel: "stylesheet",
                    href: Path.join(this.getTemporaryFolder(), "style.css")
                }
            }];
        }
        var lMetaList_arr = lHeader_obj.head.meta;
        if (aMetaInformation_obj) {
            if (aMetaInformation_obj.Title) {
                lHeader_obj.head.title = aMetaInformation_obj.Title;
            }
            aMetaInformation_obj.Author && this._pushMeta("author", aMetaInformation_obj.Author, lMetaList_arr);
            aMetaInformation_obj.Subject && this._pushMeta("subject", aMetaInformation_obj.Subject, lMetaList_arr);
            aMetaInformation_obj.Keywords && this._pushMeta("keywords", aMetaInformation_obj.Keywords, lMetaList_arr);
        }
        var lDate_obj = new Date();
        var lFormattedDate_str = [lDate_obj.getFullYear(), lDate_obj.getMonth() + 1, lDate_obj.getDate()].join("-");
        this._pushMeta("date", lFormattedDate_str, lMetaList_arr);
        return AbstractModule.makeXMLFromJSON(lHeader_obj, true);
    }

    _makeBodyPrepender(aMetaInformation_obj) {
        var lPrepender_obj = {
            div: {
                $: {
                    id: "title"
                },
            }
        };
        if (aMetaInformation_obj) {
            if (aMetaInformation_obj.Title) {
                lPrepender_obj.div.h1 = aMetaInformation_obj.Title;
            }
            if (aMetaInformation_obj.Subtitle) {
                lPrepender_obj.div.h2 = aMetaInformation_obj.Subtitle;
            }
        }
        return AbstractModule.makeXMLFromJSON(lPrepender_obj, true);
    }

    _pushMeta(aName_str, aContent_str, aMetaList_arr) {
        aMetaList_arr.push({
            $: {
                name: aName_str,
                content: aContent_str
            }
        });
    }

    _decompressTemplateIfRequired() {
        if (this.__fOptions_obj.style) {
            FS.createReadStream(this.__fOptions_obj.style)
                .pipe(UnZIP.Extract({
                    path: this.getTemporaryFolder()
                }))
                .on('close', this._onTemplateSuccessfullyExtracted.bind(this));
        } else {
            this._onTemplateSuccessfullyExtracted();
        }
    }

    _onTemplateSuccessfullyExtracted() {
        Prince({
                log: Path.join(Path.dirname(__dirname), "prince.log")
            })
            .inputs(this._fPathToOutputHTML_str)
            .output(this._fPathToOutputPDF_str)
            .execute()
            .then(this._onPDFRenderingSucceed.bind(this), this._onPDFRenderingFailed.bind(this));
    }

    _onPDFRenderingSucceed() {
        FS.unlinkSync(this._fPathToOutputHTML_str);
        this.__commandSucceed();
    }

    _onPDFRenderingFailed(aError_obj) {
        FS.unlinkSync(this._fPathToOutputHTML_str);
        this.__commandFailed(aError_obj);
    }

    static _validateFile(aFile_str) {
        var lResolvedName_str = Path.resolve(aFile_str);
        var lPath_str = Path.dirname(lResolvedName_str);
        var lPathExist_bl = AbstractModule.isIxists(lPath_str);
        if (!lPathExist_bl) {
            lResolvedName_str = Path.resolve(Path.join(process.cwd(), "output.pdf"));
        }
        return lResolvedName_str;
    }

    static renderHeading(aText, aLevel) {
        var lHeadingString = "<h" + aLevel + ">" + aText + "<h" + aLevel + "/>";
        lHeadingString = '\n<div class="heading' + aLevel + '"><div class="preheading"></div>' + lHeadingString + '<div class="postheading"></div></div>\n';
        return lHeadingString;
    }
}

module.exports = ModuleMD2PDF;