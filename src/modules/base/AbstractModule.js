"use strict";

var XML2JS = require('xml2js');
var FS = require("fs");
var Path = require("path");
var MKDirP = require('mkdirp');
var RimRaf = require('rimraf');

class AbstractModule {

	// PUBLIC API
	setCommandFinishedHandler(aHandler_func) {
		this._onCommandFinishedHandler_func = aHandler_func;
	}

	registerCommand(aNomNomParser_obj) {
		this._fNomNomCommand_obj = aNomNomParser_obj.command(this._fCommandName_str);
		this._fNomNomCommand_obj.callback(this.runCommand.bind(this));
		this._fNomNomCommand_obj
			.option('log', {
				help: "log results into [specified_path]/" + this._fCommandName_str + AbstractModule.LOG_FILENAME_APPENDIX,
				transform: AbstractModule.validateDirectory
			})
			.option('leave-tmp', {
				help: "[f] do not remove temporary folder after module completed",
				flag: true,
				default: false
			});
		this.__registerCommand(this._fNomNomCommand_obj);
	}

	getLogFilename() {
		return this._fLogFileName_str;
	}

	getCommandName() {
		return this._fCommandName_str;
	}

	getTemporaryFolder() {
		var lFolderPath_str = Path.join(AbstractModule.TEMPORARY_FOLDER_PATH, this._fCommandName_str);
		if (!AbstractModule.isIxists(lFolderPath_str)) {
			MKDirP.sync(lFolderPath_str);
		}
		return lFolderPath_str;
	}

	runCommand(aOptions_obj) {
		if (aOptions_obj.log) {
			this._fLogFileName_str = Path.join(aOptions_obj.log, this._fCommandName_str + AbstractModule.LOG_FILENAME_APPENDIX);
		}
		this._fLog_json = {
			log: {
				$: {},
				item: []
			}
		};
		this.__fLogAttributes_obj = this._fLog_json.log.$;
		this.__fLogItems_arr = this._fLog_json.log.item;
		this.__fOptions_obj = aOptions_obj;

		this.__runCommand();
	}

	constructor(aCommandName_str) {
		this._onCommandFinishedHandler_func = null;
		this._fCommandName_str = aCommandName_str;
	}

	// RPOTECTED API
	__runCommand() {
		throw new Error("must be implemented!");
	}

	__registerCommand(aNomNomCommand_obj) {
		throw new Error("must be implemented!");
	}

	__addLogItem(aLogItem_obj, aOptSorter_func) {
		if (this._fLog_json) {
			this.__fLogItems_arr.push(aLogItem_obj);
			if (aOptSorter_func) {
				this.__fLogItems_arr.sort(aOptSorter_func);
			}
		}
	}

	__setLogAttribute(aAttrName_str, aAttrValue_str) {
		if (this._fLog_json) {
			this.__fLogAttributes_obj[aAttrName_str] = aAttrValue_str;
		}
	}

	__commandSucceed() {
		if (!this.__fOptions_obj["leave-tmp"]) {
			this._cleanUp();
		}
		if (this._fLogFileName_str && this._fLog_json.log.item.length > 0) {
			var lContent_str = AbstractModule.makeXMLFromJSON(this._fLog_json);
			FS.writeFile(this._fLogFileName_str, lContent_str, this._onLogWrittenToFile.bind(this));
		} else {
			this._commandSucceed();
		}
	}

	__commandFailed(aOptMessage) {
		if (!this.__fOptions_obj["leave-tmp"]) {
			this._cleanUp();
		}
		if (this._onCommandFinishedHandler_func) {
			this._onCommandFinishedHandler_func(AbstractModule.RETCODE_FAIL, aOptMessage);
		}
	}

	__validateFile(aFilePath_str) {
		var lResolved_str = Path.resolve(aFilePath_str);
		var lExists_bl = AbstractModule.isIxists(lResolved_str);
		var lIsFile_bl = lExists_bl && FS.statSync(lResolved_str).isFile();
		if (!lExists_bl) {
			this.__commandFailed("Specified file does not exists");
		}
		if (!lIsFile_bl) {
			this.__commandFailed("Specified file is invalid");
		}
		return lResolved_str;
	}

	// PRIVATE
	_onLogWrittenToFile(aError_obj) {
		if (aError_obj) {
			this.commandFailed("Unbale to write log into " + this._fLogFileName_str + ". Reason: " + aError_obj);
		}
		this._commandSucceed();
	}

	_commandSucceed() {
		if (this._onCommandFinishedHandler_func) {
			this._onCommandFinishedHandler_func(AbstractModule.RETCODE_SUCCESS);
		}
	}

	_cleanUp() {
		var lFolderPath_str = Path.join(AbstractModule.TEMPORARY_FOLDER_PATH, this._fCommandName_str);
		if (AbstractModule.isIxists(lFolderPath_str)) {
			RimRaf.sync(lFolderPath_str);
		}
		if (AbstractModule.isIxists(AbstractModule.TEMPORARY_FOLDER_PATH)) {
			var lFilesInTempFolder_str_arr = FS.readdirSync(AbstractModule.TEMPORARY_FOLDER_PATH);
			if (lFilesInTempFolder_str_arr.length === 0) {
				FS.rmdirSync(AbstractModule.TEMPORARY_FOLDER_PATH);
			}
		}
	}

	static makeXMLFromJSON(aJSON_json, aOptOmitXMLHeader_bl) {
		var lXMLBuilder_obj = new XML2JS.Builder({
			renderOpts: {
				indent: "\t",
				pretty: true,
				newline: "\n"
			},
			xmldec: {
				'version': '1.0',
				'encoding': 'UTF-8'
			},
			headless: Boolean(aOptOmitXMLHeader_bl)
		});
		return lXMLBuilder_obj.buildObject(aJSON_json);
	}

	static isIxists(aPath_str) {
		try {
			FS.statSync(aPath_str);
		} catch (e) {
			return false;
		}
		return true;
	}

	static validateDirectory(aDirPath_str) {
		var lResolved_str = Path.resolve(aDirPath_str);
		var lExists_bl = AbstractModule.isIxists(lResolved_str);
		var lIsDirectory_bl = lExists_bl && FS.statSync(lResolved_str).isDirectory();
		if (!lExists_bl || !lIsDirectory_bl) {
			return Path.resolve(process.cwd());
		}
		return lResolved_str;
	}
}

AbstractModule.RETCODE_SUCCESS = 0;
AbstractModule.RETCODE_FAIL = 1;
AbstractModule.LOG_FILENAME_APPENDIX = ".log.xml";
AbstractModule.TEMPORARY_FOLDER_PATH = Path.join(__dirname, "../tmp");

module.exports = AbstractModule;
