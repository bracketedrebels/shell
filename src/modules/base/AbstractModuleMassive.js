"use strict";

var AbstractModule = require("./AbstractModule.js");
var XML2JS = require('xml2js');
var Path = require("path");
var FS = require("fs");
var Walk = require('walk');

class AbstractModuleMassive extends AbstractModule {
	constructor(aCommandName_str, aTargetExecutableModule_am, aOptDefaultFilenames_str_arr) {
		if (!aTargetExecutableModule_am) {
			throw new Error("No executable module provided!");
		}
		super(aCommandName_str);
		this._fUpdatetableFilesList_str_arr = null;
		this._fTargetExecutableModule_am = aTargetExecutableModule_am;
		this._fDefaultFilenames_str_arr = aOptDefaultFilenames_str_arr || [];
	}

	__runCommand() {
		this._fErrorsList_arr = [];
		this._fLogsList_arr = [];
		this.__addLogItem({
			$: {
				type: "errors"
			},
			error: this._fErrorsList_arr
		});
		this.__addLogItem({
			$: {
				type: "logs"
			},
			log: this._fLogsList_arr
		});
		this._retrieveAllUpdatableFiles();
		this._prepareTargetModule();
		this._runCommandForNextFile();
	}

	__registerCommand(aNomNomCommand_obj) {
		var lDirTransformer_func = AbstractModuleMassive._transformParamDir;
		aNomNomCommand_obj
			.option('dir', {
				abbr: 'd',
				required: true,
				help: "path to directory to walk over",
				transform: lDirTransformer_func
			})
			.option('names', {
				abbr: 'n',
				list: true,
				help: "files names to walk over. If parameter specified, only files with specified names will be walked",
				default: this._fDefaultFilenames_str_arr
			});
	}

	_retrieveAllUpdatableFiles() {
		this._fUpdatetableFilesList_str_arr = [];
		var lSelf = this;
		var lOptions_obj = {
			listeners: {
				file: this._onWalkerFileWalked.bind(lSelf),
				nodeError: this._onWalkerStatError.bind(lSelf),
				directoryError: this._onWalkerStatError.bind(lSelf)
			}
		};
		Walk.walkSync(this.__fOptions_obj.dir, lOptions_obj);
	}

	_prepareTargetModule() {
		this._fTargetExecutableModule_am.setCommandFinishedHandler(this._onPerFileCommandFinished.bind(this));
	}

	_runCommandForNextFile() {
		if (this._fUpdatetableFilesList_str_arr.length) {
			var lNextFileName_str = this._fUpdatetableFilesList_str_arr[0];
			var lOpts_obj = this.__fOptions_obj;
			var lOptions_obj = this.__getTargetCommandOptions(lNextFileName_str);
			this._fTargetExecutableModule_am.runCommand(lOptions_obj);
		} else {
			this.__commandSucceed();
		}
	}

	__getTargetCommandOptions(aFilename_str) {
		throw new Error("must be overloaded!");
	}

	_onPerFileCommandFinished(aRetCode_num, aOptMessage_str) {
		var lLogFilename_str = this._fTargetExecutableModule_am.getLogFilename();
		if (AbstractModule.isIxists(lLogFilename_str)) {
			var lLogContent_str = FS.readFileSync(lLogFilename_str);
			XML2JS.parseString(lLogContent_str, this._onLogContentParsed.bind(this));
		} else {
			this._timeToProcessNextFile();
		}
	}

	_onLogContentParsed(aErrors_obj, aResult_obj) {
		if (aErrors_obj) {
			var lErrorStat_obj = {
				type: 'file',
				name: 'log_parse_failed',
				error: aErrors_obj
			}
			this._logStatError(lErrorStat_obj);
		} else if (aResult_obj.log.item && aResult_obj.log.item.length > 0) {
			this._fLogsList_arr.push(aResult_obj.log);
		}
		this._timeToProcessNextFile();
	}

	_timeToProcessNextFile() {
		var lLogFilename_str = this._fTargetExecutableModule_am.getLogFilename();
		if (AbstractModule.isIxists(lLogFilename_str)) {
			FS.unlinkSync(lLogFilename_str);
		}
		this._fUpdatetableFilesList_str_arr.shift();
		this._runCommandForNextFile();
	}

	_onWalkerFileWalked(aRoot_str, aFileStats_obj, aNext_func) {
		var lFileName_str = Path.basename(aFileStats_obj.name);
		if (this.__fOptions_obj.names.indexOf(lFileName_str) >= 0) {
			var lReslovedPath_str = Path.resolve(aRoot_str, aFileStats_obj.name);
			this._fUpdatetableFilesList_str_arr.push(lReslovedPath_str);
		}
		aNext_func();
	}

	_onWalkerStatError(aRoot_str, aErrorStat_obj, aNext_func) {
		this._logError(aErrorStat_obj);
		aNext_func();
	}

	_logError(aErrorStat_obj) {
		var lLogErrorItem_obj = AbstractModuleMassive._makeLogErrorItem(aErrorStat_obj);
		this._fErrorsList_arr.push(lLogErrorItem_obj);
	}

	static _makeLogErrorItem(aErrorStat_obj) {
		var lErrorContent = [aErrorStat_obj.error];
		lErrorContent.$ = {
			category: aErrorStat_obj.type,
			name: aErrorStat_obj.name
		}
		return {
			error: lErrorContent
		}
	}

	static _transformParamDir(aOutputDir_str) {
		var lResolved_str = Path.resolve(aOutputDir_str);
		return FS.statSync(lResolved_str).isDirectory() ? lResolved_str : process.cwd();
	}
}

module.exports = AbstractModuleMassive;
