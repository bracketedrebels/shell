"use strict";

var AbstractModule = require("./modules/base/AbstractModule.js");
var FS = require("fs");
var Path = require("path");
var NomNom = require("nomnom");

class ModulesManager {
	constructor(aOptPathToModules_str) {
		var lDefaultPath_str = Path.join(__dirname, ModulesManager.DEFAULT_MODULES_RELATIVE_PATH);
		this._fPathToModules_str = Path.resolve(aOptPathToModules_str || lDefaultPath_str);
		this._registerModulesIfAny();
		NomNom.script("aida");
		NomNom.help("Provides an extendable set of commands. Commands can be easily extended to provide any task you want.");
	}

	processRequest() {
		NomNom.nom();
	}

	_registerModulesIfAny() {
		var lModulesFilesNames_str_arr = FS.readdirSync(this._fPathToModules_str);
		for (var i in lModulesFilesNames_str_arr) {
			this._tryToRegisterModule(Path.resolve(this._fPathToModules_str, lModulesFilesNames_str_arr[i]));
		}
	}

	_tryToRegisterModule(aModuleFilename_str) {
		var lIsFile_bl = FS.statSync(aModuleFilename_str).isFile();
		if (lIsFile_bl) {
			var lModule_func = require(aModuleFilename_str);
			this._validateModule(lModule_func) && this._registerModule(lModule_func);
		}
	}

	_validateModule(aModule_func) {
		return (aModule_func.prototype instanceof AbstractModule);
	}

	_registerModule(aModule_func) {
		var lModule_am = new aModule_func();
		lModule_am.registerCommand(NomNom);
		lModule_am.setCommandFinishedHandler(this._onSomeCommandFinished.bind(this));
	}

	_onSomeCommandFinished(aRetCode_num, aOptMessage_str) {
		if (aOptMessage_str) {
			console.log(aOptMessage_str);
			// @TODO: write to log
		}
		switch (aRetCode_num) {
			case AbstractModule.RETCODE_SUCCESS:
				{
					process.exit(0);
					break;
				}
			case AbstractModule.RETCODE_FAIL:
			default:
				{
					// @TODO: write to log
					process.exit(1);
					break;
				}
		}
	}
}

ModulesManager.DEFAULT_MODULES_RELATIVE_PATH = "modules";

module.exports = ModulesManager;
