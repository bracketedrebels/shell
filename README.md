Requirements
============

  1. Node JS (x86 or x64) (^5.3.0)
  2. NPM (^3.3.12)

Usually npm install together with nodejs, hence in most cases you need just to have nodejs installed.

Installation
============

  1. clone AIRA Shell (https://bitbucket.org/bracketedrebels/shell)
  2. npm install -g
  3. Done!
  
Usage
=====

Start using AIDA with running "aira --help" in command line.
